## create maven web project
mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-webapp -DarchetypeVersion=1.4

## create eclipse project
mvn eclipse:eclipse

## compile
mvn compile

## package
mvn war:war
mvn clean install

## import to eclipse
https://qiita.com/moris/items/812910d238314a7a3a47

## Struts 2, Spring 4, Hibernate 4 and Maven Integration
https://www.jeejava.com/struts-2-spring-4-hibernate-4-and-maven-integration/

## git submodule
https://www.vogella.com/tutorials/GitSubmodules/article.html