package com.servicememe.sshDemo.spring.dao;

import java.util.List;
import com.servicememe.sshDemo.hibernate.model.UserDetails;

public interface UserDetailsDao {

    public UserDetails getuserDetails(int id);

    public List<UserDetails> getAllUserDetails();

}