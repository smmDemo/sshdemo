package com.servicememe.sshDemo.spring.service;

import java.util.List;
import com.servicememe.sshDemo.hibernate.model.UserDetails;

public interface UserDetailsService {

    public UserDetails getuserDetails(int id);

    public List<UserDetails> getAllUserDetails();

}