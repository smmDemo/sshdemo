package com.servicememe.sshDemo.struts.action;

import java.util.List;

import org.apache.struts2.dispatcher.DefaultActionSupport;
import org.springframework.beans.factory.annotation.Autowired;

import com.servicememe.sshDemo.hibernate.model.UserDetails;
import com.servicememe.sshDemo.spring.service.UserDetailsService;

public class UserDetailsAction extends DefaultActionSupport {

    private static final long serialVersionUID = 1L;

    @Autowired
    private UserDetailsService userDetailsService;

    private UserDetails userDetails;
    private List<UserDetails> userDetailsList;

    public String getAUserDetails() {
        if (userDetails == null) {
            int id = 7;// should come from UI
            userDetails = userDetailsService.getuserDetails(id);
        }
        return SUCCESS;
    }

    public String getAllUserDetails() {
        if (userDetailsList == null) {
            userDetailsList = userDetailsService.getAllUserDetails();
        }
        return SUCCESS;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public List<UserDetails> getUserDetailsList() {
        return userDetailsList;
    }

    public void setUserDetailsList(List<UserDetails> userDetailsList) {
        this.userDetailsList = userDetailsList;
    }

    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

}