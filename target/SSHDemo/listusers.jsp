<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Struts 2, Spring 4 and Hibernate 4 Integration Example</title>
<s:if test="userDetailsList.size() > 0">
    <h2>List of User Details</h2>
    <table cellpadding="5px" border="1px">
        <tbody>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>DOB</th>
            </tr>
            <s:iterator value="userDetailsList">
                <tr>
                    <td><s:property value="id"></s:property></td>
                    <td><s:property value="firstName"></s:property></td>
                    <td><s:property value="lastName"></s:property></td>
                    <td><s:property value="email"></s:property></td>
                    <td><s:property value="dob"></s:property></td>
                </tr>
            </s:iterator>
        </tbody>
    </table>
</s:if>
</body>
</html>