<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Struts 2, Spring 4 and Hibernate 4 Integration Example</title>
<s:if test="userDetails != null">
    <h2>User Details</h2>
    <table cellpadding="5px" border="1px">
        <tbody>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>DOB</th>
            </tr>
            <tr>
                <td><s:property value="userDetails.id"></s:property></td>
                <td><s:property value="userDetails.firstName"></s:property></td>
                <td><s:property value="userDetails.lastName"></s:property></td>
                <td><s:property value="userDetails.email"></s:property></td>
                <td><s:property value="userDetails.dob"></s:property></td>
            </tr>
        </tbody>
    </table>
</s:if>
</body>
</html>